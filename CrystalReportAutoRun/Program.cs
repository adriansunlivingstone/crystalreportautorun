﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrystalReportAutoRun
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {

                TaskMgr taskMgr = new TaskMgr();

                for (int i = 0; i < taskMgr.ReportTasks.Count; i++)
                {
                    TaskInfo task = taskMgr.ReportTasks[i];

                    ReportDocument cryRpt = new ReportDocument();
                    //Load the report from the spefic path
                    cryRpt.Load(task.CrystalReportFilePath);
                    cryRpt.Refresh();

                    //if patr ameter is not null then add the patrameter name and value to the crystal report
                    if (task.Parameters != null)
                    {
                        foreach (TaskParameter patra in task.Parameters)
                        {
                            if(patra.ParameterValue !=null )
                            {
                               cryRpt.SetParameterValue(patra.ParameterName, patra.ParameterValue);
                            }
                            else
                            {
                                cryRpt.SetParameterValue(patra.ParameterName, patra.ParameterValues);
                            }

                           
                        }
                    }

                    //export the crytal report as excel file 
                    cryRpt.ExportToDisk(ExportFormatType.Excel, Tool.GetExcelFileName(Tool.ReadSetting("ExportReportPathLocation"), task.ReportLoc, task.ExportFileName));
                    cryRpt.Dispose();


                }




            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
             
            }
        }



    
    }
}
