﻿using Livingstone.DBLib;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrystalReportAutoRun
{
    public class TaskMgr
    {
        private static readonly IDBHandler dBHandler = new SqlServerDB();
        private List<TaskInfo> reportTasks;

        public List<TaskInfo> ReportTasks
        {
            get { return reportTasks; }
            set { reportTasks = value; }
        }
        public TaskMgr()
        {
            reportTasks = new List<TaskInfo>();
            LoadReports();
        }

        //private void LoadReports()
        //{
        //        DataTable dt = new DataTable();

        //        string sql = @"Select Slspsn_No, upper(  isnull(usr, '')   ) usr, Slspsn_Name
        //                        from CusSlspsn
        //                        order by case when usr is null then 1 else  0 end, usr asc, Slspsn_No asc
        //                        ";

        //        dBHandler.getDataList(dt, sql, SqlServerDB.Servers.netcrmau);


        //    if (dt.Rows.Count > 0)
        //    {
        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {
        //            String SalesRep = dt.Rows[i][0].ToString().Trim();

        //            List<TaskParameter> parameters = new List<TaskParameter>();
        //            parameters.Add(new TaskParameter("Pre-defined Date", "None"));
        //            parameters.Add(new TaskParameter("Start Date", "01/07/2019"));
        //            parameters.Add(new TaskParameter("End Date", "30/06/2020"));
        //            parameters.Add(new TaskParameter("Product Category", "All"));
        //            parameters.Add(new TaskParameter("Affinity From", ""));
        //            parameters.Add(new TaskParameter("Affinity To", ""));
        //            parameters.Add(new TaskParameter("Sales Rep From", SalesRep));
        //            parameters.Add(new TaskParameter("Sales Rep To", SalesRep));
        //            reportTasks.Add(new TaskInfo("Sales By Cus Code With Growth With Rep No", parameters, "AU", SalesRep, GetFilePath("AU", "Sales By Cus Code With Growth With Rep No")));
        //        }
        //    }
        //}

        //private string GetFilePath(string location, string fileName)
        //{
        //    if (location.ToUpper() == "HK")
        //        return @"C:\Users\adrian_sun\Desktop\reports\CrystalReportFile\" + fileName + ".rpt";
        //    else
        //        return @"C:\Users\adrian_sun\Desktop\reports\CrystalReportFile\" + fileName + ".rpt";
        //}



        private void LoadReports()
        {
            List<TaskParameter> parameters = new List<TaskParameter>();
            string dateInfo = DateTime.Now.Year.ToString() + DateTime.Now.ToString("MM") + DateTime.Now.ToString("dd");
            Object[] whs =  { "BA","BC","CM","CV","NF","P","PC","PM","R","Y","T","TT","V","VK" }; 
            //"BA,BC,CM,CV,NF,P,PC,PM,R,Y,T,TT,V,VK"" 'BA','BC','CM','CV','NF','P','PC','PM','R','Y','T','TT','V','VK'"

            parameters.Add(new TaskParameter("Years", "8"));
            parameters.Add(new TaskParameter("Warehouse", "Stock WH"));
            parameters.Add(new TaskParameter("Warehouse List", whs));
            reportTasks.Add(new TaskInfo("Stock Value by Grp_detailed_pprice_v4_BKdb", parameters, "AU", "Stock Value by Grp_detailed_pprice_v4_BKdb_for_Marcus_"+ dateInfo, GetFilePath("AU", "Stock Value by Grp_detailed_pprice_v4_BKdb")));

        }


        private string GetFilePath(string location, string fileName)
        {
            if (location.ToUpper() == "HK")
                return @"C:\Users\adrian_sun\Desktop\reports\CrystalReportFile\" + fileName + ".rpt";
            else
                return @"C:\Users\adrian_sun\Desktop\reports\CrystalReportFile\" + fileName + ".rpt";
        }













    }
}
