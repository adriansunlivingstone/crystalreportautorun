﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrystalReportAutoRun
{
    public class TaskParameter
    {
        private string parameterName;

        public string ParameterName
        {
            get { return parameterName; }
            set { parameterName = value; }
        }

        private string parameterValue;
        private Object[] parameterValues;

        public string ParameterValue
        {
            get { return parameterValue; }
            set { parameterValue = value; }
        }
        public Object[] ParameterValues
        {
            get { return parameterValues; }
            set { parameterValues = value; }
        }

        public TaskParameter()
        {
        }

        public TaskParameter(string parameterName, string parameterValue)
        {
            this.parameterName = parameterName;
            this.parameterValue = parameterValue;
        }
        public TaskParameter(string parameterName, Object[] paras)
        {
            this.parameterName = parameterName;
            this.parameterValues = paras;
        }
    }
}
