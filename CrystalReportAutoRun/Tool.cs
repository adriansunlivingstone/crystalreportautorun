﻿using Livingstone.DBLib;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrystalReportAutoRun
{
    public class Tool
    {
        private static readonly IDBHandler dBHandler = new SqlServerDB();
        public static String folder = "";
        public static string ReadSetting(string key)
        {
            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                string result = appSettings[key] ?? "Not Found";
                return result;
            }
            catch (ConfigurationErrorsException)
            {
                return string.Empty;
            }
        }
        /// <summary>Get excel file name for export file </summary>
        /// <param name="location">HK or AU</param>
        /// <param name="fileName">excel file name you want to speficy</param>
        /// <param name="path">excel file path you want to save</param>
        /// <returns>excel file name</returns>
        public static string GetExcelFileName(string path, string location, string fileName)
        {

            if (folder == "")
            {
                folder = path + DateTime.Now.Year.ToString() + DateTime.Now.ToString("MM") + DateTime.Now.ToString("dd") + "_" + DateTime.Now.Hour.ToString()+"_"+DateTime.Now.Minute;
            }

            //folder will not change for the program, it's date and time,so it is static field in the Tool class. while report folder is the current report's folder, check everytime. 
            string reportFolder = "";

            string SalesRepName = GetSalesRepName(fileName);
            if (!String.IsNullOrEmpty(SalesRepName))
            {
                reportFolder = folder + "\\" + SalesRepName + "\\";
            }
            else
            {
                reportFolder = folder;
            }



            if (Directory.Exists(reportFolder) == false)
                Directory.CreateDirectory(reportFolder);
            //set export file path
            fileName = reportFolder + "\\" + fileName;



            if (location.ToUpper() == "HK")
                return fileName + " hk.xls";
            else
                return fileName + ".xls";
        }

        private static string GetSalesRepName(string repNo)
        {
            String SlspsnName = "";
            Dictionary<string, object> param = new Dictionary<string, object>()
                {
                    {"@Rep",repNo  }
                };
            DataTable dt = new DataTable();

            string sql = @"Select top 1 Slspsn_No, upper(  isnull(usr, '')   ) usr, Slspsn_Name
                            from CusSlspsn
                            where Slspsn_No = @Rep ";

            dBHandler.getDataList(dt, sql, SqlServerDB.Servers.netcrmau, param);

            if (dt.Rows.Count > 0)
            {
                SlspsnName = dt.Rows[0][1].ToString().Trim();
            }
            return SlspsnName;
        }



    }
}
